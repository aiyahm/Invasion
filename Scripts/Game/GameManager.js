const BG = [0, 0, 10], FULL = 600, BINT = 10, MENU = 1, GAME = 2;
let aliens, bullets, hero, bullet, lives, finalScore, score, mxPowerInt = 20, mxAliens = [0, 0, 0], curAliens = [0, 0, 0], pulseCharge = 1;
let bulletInt = BINT, actShoot = true, currentScreen = MENU, playButton;
let bulletIMG, pulseIMG, alienIMG, heroIMG, splashIMG, logo, instructions, shootSND, explodeSND, musicBG, powerInterval;
function preload() {
    alienIMG = [loadAnimation("Assets/Sprites/Enemy_A/Enemy_A_1.png", "Assets/Sprites/Enemy_A/Enemy_A_6.png"),
    loadAnimation("Assets/Sprites/Enemy_B/Enemy_B_1.png", "Assets/Sprites/Enemy_B/Enemy_B_6.png"),
    loadAnimation("Assets/Sprites/Enemy_C/Enemy_C_1.png", "Assets/Sprites/Enemy_C/Enemy_C_6.png")]
    logo = loadImage("Assets/UI/Logo.png");
    heroIMG = loadImage("Assets/Sprites/Hero.png");
    lifeIMG = loadImage("Assets/Sprites/Lives.png");
    powerIMG = [loadImage("Assets/Sprites/LifePowerup.png"), loadImage("Assets/Sprites/PulsePowerup.png")];
    bulletIMG = loadImage("Assets/Sprites/Bullet.png");
    pulseIMG = loadImage("Assets/Sprites/Pulse.png");
    instructions = loadImage("Assets/UI/Controls.png");
    shootSND = loadSound("Assets/UI/Laser_Gun.m4a");
    explodeSND = loadSound("Assets/UI/Explode.mp3");
    musicBG = loadSound("Assets/UI/Powerup.mp3");
}
function setupSprites() {
    shootSND.setVolume(0.5); explodeSND.setVolume(0.1);
    heros = new Group();
    aliens = new Group();
    bullets = new Group();
    pulses = new Group();
    powers = new Group();
    let hero = createSprite(0, 0, 100, 100);
    hero.sX = random(0, 5); hero.sY = random(0, 5); hero.mX = hero.sX; hero.mY = hero.sX;
    hero.addImage(heroIMG);
    hero.addToGroup(heros);
}
function setup() {
    frameRate(60);
    createCanvas(windowWidth - 10, windowHeight - 10);
    background(BG);
    setupSprites();
}
function drawHUD() {
    for (i = 0; i <= lives; i++) { image(lifeIMG, 0 + i * 60, 10); lifeIMG.resize(50, 50); }
    fill(255, 20, 20);
    rect(0, height - 10, map(pulseCharge, 0, FULL, 0, width), 10);
    fill(0, 255, 0); noStroke(); textSize(50); textAlign(RIGHT);
    text("hits: " + score, width - 50, 50); text("time: " + floor(frameCount / 60), width - 50, 100);
}
function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
    background(BG);
}
function draw() {
    if (currentScreen == GAME) { drawGame(); }
    else { drawMenu(); }
}

