function resetGame() {
    clear(); removeElements();
}
function drawMenu() {
    resetGame();
    background(BG);
    image(logo, width / 2 - (341), height / 2 - 400, 341 * 2, 150 * 2);
    if (finalScore > 0) {
        playButton = createButton("Replay");
    } else { playButton = createButton("Play"); }
    playButton.style('font-size', 50 + "px");
    playButton.size(200, 100);
    playButton.position(width / 2 - 100, height / 2);
    playButton.mousePressed(beginGame);
    image(instructions, width / 2 - (100 * (1349 / 268)) / 2, height - 100, 100 * (1349 / 268), 100);
    if (finalScore > 0) { fill(0, 255, 0); noStroke(); textSize(50); textAlign(RIGHT); text("Score: " + finalScore, width - 50, 50); }
    score = 0; lives = 4;
}
function drawGame() {
    if (currentScreen == GAME) {
        background(BG);
        difficultyHandler();
        newAliens();
        alienMove(); heroMove(); powerMove();
        bulletRemove(); drawHUD();
        if (powerInterval <= 0) {
            pType = floor(random(0, 1.9));
            createPowerUp(pType);
            print("powerup: " + String(pType));
            powerInterval = 60 * random(10, mxPowerInt);
        } else { powerInterval--; }
        drawSprites();
    }
}
function beginGame() {
    resetGame();
    currentScreen = GAME;
    newAliens();
    if (heros.length < 1) {
        frameCount = 0;
        score = 0; lives = 4; pulseCharge = 1
        setupSprites();
    }
    heros[0].position = { x: width / 2, y: height - 70 };
    musicBG.stop(); musicBG.setVolume(0.075); musicBG.play; musicBG.loop();
    powerInterval = 60 * random(10, mxPowerInt);

}
function gameOver() {
    currentScreen = MENU;
    aliens.removeSprites();
    curAliens = [0, 0, 0]; mxAliens = [10, 0, 0];
    powers.removeSprites();
    heros.removeSprites();
    bullets.removeSprites();
    pulses.removeSprites();
    resetGame();
}