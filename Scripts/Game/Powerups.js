function createPowerUp(pType) {
    if (currentScreen == GAME) {
        power = createSprite(0, 0, 100, 100);
        power.pType = pType
        let direction = random(-1, 1);
        if (direction < 0) {
            power.sX = 0 - random(2, 8);
        } else power.sX = random(2, 8);
        power.sY = random(2, 8);
        power.mX = power.sX; power.mY = power.sX;
        power.position = { x: random(50, width - 50), y: -100 };
        power.addImage(powerIMG[pType]);
        power.setDefaultCollider();
        power.addToGroup(powers);
    }
}
function powerActivate() {
    if (power.pType < 1) { lives++; }
    else { pulseCharge = FULL; }
    power.remove(); bullet.remove();
}
function powerMove() {
    if (currentScreen == GAME) {
        powers.overlap(bullets, powerActivate);
        powers.overlap(pulses, powerActivate);
        powers.overlap(heros, powerActivate);
        for (i = 0; i < powers.length; i++) {
            power = powers[i];
            if (power.position.x - 50 <= 0) { power.mX = 0 - power.mX; }
            else if (power.position.x + 50 >= width) { power.mX = 0 - power.mX; }
            if (power.position.y - 50 <= 0) { power.mY = power.sY; }
            power.setVelocity(power.mX, power.mY);
        }
    }
}