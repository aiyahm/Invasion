function heroMove() {
    if (currentScreen == GAME) {
        hero = heros[0];
        if (keyIsDown(LEFT_ARROW)) { hero.rotation = -10; hero.position.x -= 10; }
        if (keyIsDown(RIGHT_ARROW)) { hero.rotation = 10; hero.position.x += 10; }
        if (keyIsDown(65)) { hero.rotation = -10; hero.position.x -= 10; }
        if (keyIsDown(68)) { hero.rotation = 10; hero.position.x += 10; }
        if (hero.position.x < -100) { hero.position.x = width + 100 }
        if (hero.position.x > width + 100) { hero.position.x = -100 }
        if (heros.length == 1) {
            if (keyIsPressed == true && keyCode == 32) { if (actShoot == true) { shoot(); actShoot = false; } }
            if (keyIsPressed == true && keyCode == 16) { if (pulseCharge >= FULL) { shootPulse(); } }
        }
        if (pulseCharge < FULL) { pulseCharge += 1 }
    }
}
function shoot() {
    shootSND.play();
    bullet = createSprite(hero.position.x, hero.position.y - 64, 32, 100); bullet.setVelocity(0, -50);
    bullet.setDefaultCollider(); bullet.addImage(bulletIMG);
    bullet.addToGroup(bullets); bulletInt = 0;
}
function shootPulse() {
    shootSND.play();
    pulse = createSprite(hero.position.x, hero.position.y - 41, 32, 100); pulse.setVelocity(0, -50);
    pulse.setDefaultCollider();
    pulse.addImage(pulseIMG);
    pulse.addToGroup(pulses);
    pulseCharge = 1;
}
function keyReleased() {
    hero.rotation = 0; if (keyCode == 32 || keyCode == 16) { actShoot = true; }
}
function difficultyHandler() {
    if (score <= 30) { mxAliens = [10, 0, 0]; mxPowerInt = 60; }
    else if (score > 30 && score <= 40) { mxAliens = [10, 5, 0]; mxPowerInt = 50; }
    else if (score > 40 && score <= 50) { mxAliens = [10, 7, 0]; mxPowerInt = 40; }
    else if (score > 50 && score <= 70) { mxAliens = [10, 10, 0]; mxPowerInt = 30; }
    else if (score > 70 && score <= 90) { mxAliens = [10, 10, 5]; mxPowerInt = 20; }
    else if (score > 90 && score <= 110) { mxAliens = [10, 10, 7]; }
    else { mxAliens = [10, 10, 10]; }
}
function bulletRemove() {
    if (bullets.length > 0) { for (i = 0; i < bullets.length; i++) { s = bullets[i]; if (bullet.position.y <= -100) { bullet.remove(); } } }
    if (pulses.length > 0) { for (i = 0; i < pulses.length; i++) { s = pulses[i]; if (bullet.position.y <= -100) { bullet.remove(); } } }
}
function lifeloss(alien, hero) {
    explodeSND.play();
    if (lives >= 1) { alien.remove(); lives--; curAliens[alien.eType]--; }
    else if (lives == 0) { gameOver(); finalScore = score * floor(frameCount / 60); }
}