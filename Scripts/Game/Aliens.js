function newAliens() {
    if (currentScreen == GAME) {
        for (i = 0; i < mxAliens.length; i++) {
            if (curAliens[i] < mxAliens[i]) {
                alien = createSprite(0, 0, 100, 100);
                alien.eType = i
                print("created alien: " + alien.eType);
                let direction = random(-1, 1);
                if (direction < 0) { alien.sX = 0 - random(i + 2, i + 5); }
                else alien.sX = random(i + 2, i + 5);
                alien.sY = random(i + 2, i + 5);
                alien.mX = alien.sX; alien.mY = alien.sX;
                alien.position = { x: random(50, width - 50), y: -100 };
                alien.addAnimation('alive', alienIMG[i]);
                alien.setDefaultCollider();
                alien.addToGroup(aliens);
                curAliens[i]++;
                print(curAliens);
            }
        }
    }
}
function alienMove() {
    if (currentScreen == GAME) {
        aliens.overlap(bullets, alienDie);
        aliens.overlap(pulses, alienDie);
        aliens.overlap(heros, lifeloss);
        for (i = 0; i < aliens.length; i++) {
            alien = aliens[i];
            if (alien.position.x - 50 <= 0) { alien.mX = 0 - alien.mX; }
            else if (alien.position.x + 50 >= width) { alien.mX = 0 - alien.mX; }
            if (alien.position.y - 50 <= 0) { alien.mY = alien.sY; }
            else if (alien.position.y + 50 >= height) { alien.mY = -alien.sY; }
            alien.setVelocity(alien.mX, alien.mY);
        }
    }
}
function alienDie(alien, bullet) {
    explodeSND.play();
    alien.remove();
    if (pulses.length <= 0) bullet.remove();
    curAliens[alien.eType]--;
    print("alien:" + alien.etype + " died");
    score += (alien.eType + 1);
}
